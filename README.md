# Code Foundry stopwatch API

## Installing & running the example server

Excute the following command to install the NPM dependencies:

```shell
npm install
```

After the NPM dependencies have been installed successfully, you can run the server with the following command:

```shell
npm start
```

This should start the NestJS server on port 3000.

## API specs

The API is specified using OpenAPI 3.0.3 in `yaml` format and can be found in the [`stopwatch-api.yaml`]('./stopwatch-api.yaml') file.

To view the API spec (and try it out) you can import it in the online [Swagger Editor](https://editor.swagger.io/).

The example server has CORS support, so you should have no CORS issues when trying out the examples using the online Swagger UI.
