import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  NotFoundException,
  Param,
  Post,
  Put,
  HttpStatus,
  Patch,
} from '@nestjs/common';
import { SessionService } from './services';
import { NewSessionDto, PartialSessionDto, StoredSessionDto } from './models/session.dto.model';
import {
  deserializeNewSession,
  deserializePartialSession,
  deserializeStoredSession,
  serializeStoredSession,
} from './converters/session.converter';
import { deserializeDateTime } from './converters/date-time.converter';

@Controller('api/sessions')
export class SessionsController {
  constructor(private readonly sessionService: SessionService) {}

  @Get() public getAllSessions(): StoredSessionDto[] {
    return this.sessionService.getSessions().map(serializeStoredSession);
  }

  @Get(':sessionId') public getSessionById(@Param('sessionId') sessionId: string): StoredSessionDto {
    const session = this.sessionService.getSession(sessionId);

    if (!session) {
      throw new NotFoundException(undefined, 'Session not found');
    }

    return serializeStoredSession(session);
  }

  @Post() public createSession(@Body() newSessionDto: NewSessionDto): StoredSessionDto {
    const session = this.sessionService.createSession(deserializeNewSession(newSessionDto));

    return serializeStoredSession(session);
  }

  @Put(':sessionId') public updateSession(
    @Param('sessionId') sessionId: string,
    @Body() updatedSessionDto: StoredSessionDto,
  ): StoredSessionDto {
    const updatedSession = deserializeStoredSession(updatedSessionDto);

    if (updatedSession.id !== sessionId) {
      throw new BadRequestException(undefined, 'Session id in URL does not match with session id in request body');
    }

    const storedSession = this.sessionService.updateSession(updatedSession);

    if (!storedSession) {
      throw new NotFoundException(undefined, 'Session not found');
    }

    return serializeStoredSession(storedSession);
  }

  @Patch(':sessionId') public patchSession(
    @Param('sessionId') sessionId: string,
    @Body() partialSessionDto: PartialSessionDto,
  ): StoredSessionDto {
    const partialSession = deserializePartialSession(partialSessionDto);

    const storedSession = this.sessionService.patchSession(sessionId, partialSession);

    if (!storedSession) {
      throw new NotFoundException(undefined, 'Session not found');
    }

    return serializeStoredSession(storedSession);
  }

  @Delete(':sessionId') @HttpCode(HttpStatus.NO_CONTENT) public deleteSession(@Param('sessionId') sessionId: string): void {
    const sessionWasDeleted = this.sessionService.deleteSession(sessionId);

    if (!sessionWasDeleted) {
      throw new NotFoundException(undefined, 'Session not found');
    }
  }

  @Put(':sessionId/start') public startSession(
    @Param('sessionId') sessionId: string,
    @Body() startRequest?: { startedAt: string },
  ): StoredSessionDto {
    const startedAt = startRequest?.startedAt ? deserializeDateTime(startRequest.startedAt) : undefined;

    const storedSession = this.sessionService.startSession(sessionId, startedAt);

    if (!storedSession) {
      throw new NotFoundException(undefined, 'Session not found');
    }

    return serializeStoredSession(storedSession);
  }

  @Put(':sessionId/pause') public pauseSession(
    @Param('sessionId') sessionId: string,
    @Body() { resumptionTime }: { resumptionTime: number },
  ): StoredSessionDto {
    const storedSession = this.sessionService.pauseSession(sessionId, resumptionTime);

    if (!storedSession) {
      throw new NotFoundException(undefined, 'Session not found');
    }

    return serializeStoredSession(storedSession);
  }

  @Put(':sessionId/finish') public finishSession(@Param('sessionId') sessionId: string): StoredSessionDto {
    const storedSession = this.sessionService.finishSession(sessionId);

    if (!storedSession) {
      throw new NotFoundException(undefined, 'Session not found');
    }

    return serializeStoredSession(storedSession);
  }

  @Post(':sessionId/laps') @HttpCode(HttpStatus.NO_CONTENT) public recordLapTime(
    @Param('sessionId') sessionId: string,
    @Body() { lap: lapTimeInMilliseconds }: { lap: number },
  ): void {
    const updatedSession = this.sessionService.recordLapTime(sessionId, lapTimeInMilliseconds);

    if (!updatedSession) {
      throw new NotFoundException(undefined, 'Session not found');
    }
  }
}
