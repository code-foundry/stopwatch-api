import { Module } from '@nestjs/common';

import { SessionService } from './services';

import { SessionsController } from './sessions.controller';

@Module({
  imports: [],
  controllers: [SessionsController],
  providers: [SessionService],
})
export class SessionsModule {}
