import { Injectable } from '@nestjs/common';
import { NewSession, PartialSession, StoredSession } from '../models';
import { BehaviorSubject } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SessionService {
  private readonly sessionsSubject = new BehaviorSubject<StoredSession[]>([
    {
      id: '66242faa-bbde-401f-87c3-f5a394c417e9',
      name: 'Finished session',
      finished: true,
      laps: [1234, 5678, 60000],
    },
  ]);

  public getSessions(): StoredSession[] {
    return this.sessions;
  }

  public getSession(sessionId: string): StoredSession | undefined {
    return this.sessions.find(({ id }) => id === sessionId);
  }

  public createSession(newSession: NewSession): StoredSession {
    const newStoredSession: StoredSession = {
      id: uuidv4(),
      name: newSession.name,
      finished: false,
      laps: [],
    };

    this.sessions = [...this.sessions, newStoredSession];

    return newStoredSession;
  }

  public updateSession(updatedSession: StoredSession): StoredSession | undefined {
    const existingSession = this.sessions.find(({ id }) => id === updatedSession.id);

    if (!existingSession) {
      return undefined;
    }

    this.storeUpdatedSession(updatedSession);

    return updatedSession;
  }

  public patchSession(sessionId: string, updates: PartialSession): StoredSession | undefined {
    const existingSession = this.sessions.find(({ id }) => id === sessionId);

    if (!existingSession) {
      return undefined;
    }

    const updatedSession = {
      ...existingSession,
      ...updates,
    };

    this.storeUpdatedSession(updatedSession);

    return updatedSession;
  }

  public deleteSession(sessionId: string): boolean {
    const newSessions = this.sessions.filter(({ id }) => id !== sessionId);

    const sessionWasFound = newSessions.length < this.sessions.length;

    if (sessionWasFound) {
      this.sessions = newSessions;
    }

    return sessionWasFound;
  }

  public startSession(sessionId: string, startedAt?: Date): StoredSession | undefined {
    const existingSession = this.sessions.find(({ id }) => id === sessionId);

    if (!existingSession) {
      return undefined;
    }

    const updatedSession = {
      ...existingSession,
      startedAt: startedAt ?? new Date(),
      resumptionTime: undefined,
    };

    this.storeUpdatedSession(updatedSession);

    return updatedSession;
  }

  public pauseSession(sessionId: string, resumptionTime: number): StoredSession | undefined {
    const existingSession = this.sessions.find(({ id }) => id === sessionId);

    if (!existingSession) {
      return undefined;
    }

    const updatedSession = {
      ...existingSession,
      startedAt: undefined,
      resumptionTime,
    };

    this.storeUpdatedSession(updatedSession);

    return updatedSession;
  }

  public finishSession(sessionId: string): StoredSession | undefined {
    const existingSession = this.sessions.find(({ id }) => id === sessionId);

    if (!existingSession) {
      return undefined;
    }

    const updatedSession = {
      ...existingSession,
      startedAt: undefined,
      resumptionTime: undefined,
      finished: true,
    };

    this.storeUpdatedSession(updatedSession);

    return updatedSession;
  }

  public recordLapTime(sessionId: string, lapTimeInMilliseconds: number): StoredSession | undefined {
    const existingSession = this.sessions.find(({ id }) => id === sessionId);

    if (!existingSession) {
      return undefined;
    }

    const updatedSession = {
      ...existingSession,
      laps: [...existingSession.laps, lapTimeInMilliseconds].sort((a, b) => a - b),
    };

    this.storeUpdatedSession(updatedSession);

    return updatedSession;
  }

  private storeUpdatedSession(updatedSession: StoredSession): void {
    this.sessions = this.sessions.map((session) => (session.id === updatedSession.id ? updatedSession : session));
  }

  private get sessions(): StoredSession[] {
    return this.sessionsSubject.value;
  }

  private set sessions(newSessions: StoredSession[]) {
    this.sessionsSubject.next(newSessions);
  }
}
