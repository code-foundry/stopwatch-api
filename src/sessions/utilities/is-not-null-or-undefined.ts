export function isNotNullOrUndefined<TValue>(value: TValue): value is Exclude<TValue, null | undefined> {
  return value !== null && value !== undefined;
}
