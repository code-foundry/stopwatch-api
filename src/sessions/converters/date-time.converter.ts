export function deserializeDateTime(iso8601DateTime: string): Date {
  const result = new Date(iso8601DateTime);

  if (Number.isNaN(result.getTime())) {
    throw new Error(
      `Cannot deserialize ${iso8601DateTime} as a valid date time`,
    );
  }

  return result;
}

export function serializeDateTime(dateTime: Date): string {
  return dateTime.toISOString();
}
