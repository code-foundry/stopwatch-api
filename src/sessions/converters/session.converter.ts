import { NewSession, PartialSession, Session, StoredSession } from '../models';
import { NewSessionDto, PartialSessionDto, SessionDto, StoredSessionDto } from '../models/session.dto.model';
import { isNotNullOrUndefined } from '../utilities';
import { deserializeDateTime, serializeDateTime } from './date-time.converter';

export function serializeSession(session: Session): SessionDto {
  return {
    name: session.name,
    ...('startedAt' in session
      ? {
          startedAt: session.startedAt !== undefined ? serializeDateTime(session.startedAt) : undefined,
        }
      : {}),
    ...('resumptionTime' in session ? { resumptionTime: session.resumptionTime } : {}),
    finished: session.finished,
    laps: session.laps,
  };
}

export function deserializeSession(sessionDto: SessionDto): Session {
  return {
    name: sessionDto.name,
    ...('startedAt' in sessionDto
      ? {
          startedAt: sessionDto.startedAt !== undefined ? deserializeDateTime(sessionDto.startedAt) : undefined,
        }
      : {}),
    ...('resumptionTime' in sessionDto ? { resumptionTime: sessionDto.resumptionTime } : {}),
    finished: sessionDto.finished,
    laps: sessionDto.laps,
  };
}

export function deserializePartialSession(partialSessionDto: PartialSessionDto): PartialSession {
  return {
    ...(isNotNullOrUndefined(partialSessionDto.name) ? { name: partialSessionDto.name } : {}),
    ...('startedAt' in partialSessionDto
      ? { startedAt: isNotNullOrUndefined(partialSessionDto.startedAt) ? deserializeDateTime(partialSessionDto.startedAt) : undefined }
      : {}),
    ...('resumptionTime' in partialSessionDto
      ? { resumptionTime: isNotNullOrUndefined(partialSessionDto.resumptionTime) ? partialSessionDto.resumptionTime : undefined }
      : {}),
    ...(isNotNullOrUndefined(partialSessionDto.finished) ? { finished: partialSessionDto.finished } : {}),
    ...(isNotNullOrUndefined(partialSessionDto.laps) ? { laps: partialSessionDto.laps } : {}),
  };
}

export function deserializeNewSession(newSessionDto: NewSessionDto): NewSession {
  return {
    name: newSessionDto.name,
  };
}

export function serializeStoredSession(storedSession: StoredSession): StoredSessionDto {
  return {
    id: storedSession.id,
    ...serializeSession(storedSession),
  };
}

export function deserializeStoredSession(storedSessionDto: StoredSessionDto): StoredSession {
  return {
    id: storedSessionDto.id,
    ...deserializeSession(storedSessionDto),
  };
}
