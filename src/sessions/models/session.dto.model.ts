export interface SessionDto {
  name: string;
  startedAt?: string;
  resumptionTime?: number;
  finished: boolean;
  laps: number[];
}

export type NewSessionDto = Pick<SessionDto, 'name'>;

export type PartialSessionDto = Partial<SessionDto>;

export interface StoredSessionDto extends SessionDto {
  id: string;
}
