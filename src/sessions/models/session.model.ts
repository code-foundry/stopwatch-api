export interface Session {
  name: string;
  startedAt?: Date;
  resumptionTime?: number;
  finished: boolean;
  laps: number[];
}

export type NewSession = Pick<Session, 'name'>;

export type PartialSession = Partial<Session>;

export interface StoredSession extends Session {
  id: string;
}
