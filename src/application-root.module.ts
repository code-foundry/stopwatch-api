import { Module } from '@nestjs/common';

import { SessionsModule } from './sessions';

@Module({
  imports: [SessionsModule],
})
export class ApplicationRootModule {}
