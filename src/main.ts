import { NestFactory } from '@nestjs/core';
import { ApplicationRootModule } from './application-root.module';

async function bootstrap() {
  const app = await NestFactory.create(ApplicationRootModule, { cors: true });
  await app.listen(3000);
}
bootstrap();
